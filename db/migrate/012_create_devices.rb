class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :name, default: ""
      t.string :state, default: ""
      t.text :comment, default: ""
      t.string :summary, default: ""
      t.text :description, default: ""
      t.string :website, default: ""
      t.string :category, default: ""
      t.string :contact_name, default: ""
      t.string :contact_email, default: ""
      t.date :out_of_date
      t.date :last_vendor_update
      t.references :vendor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
