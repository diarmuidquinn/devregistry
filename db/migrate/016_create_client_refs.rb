class CreateClientRefs < ActiveRecord::Migration
  def change
    create_table :client_refs do |t|
      t.string :name, default: ""
      t.string :state, default: ""
      t.text :comment, default: ""
      t.string :division, default: ""
      t.string :website, default: ""
      t.string :summary, default: ""
      t.text :description, default: ""
      t.string :contact_name, default: ""
      t.string :contact_email, default: ""
      t.boolean :permitted, default: false
      t.date :out_of_date
      t.date :last_vendor_update
      t.references :device, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
