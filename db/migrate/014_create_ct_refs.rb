class CreateCtRefs < ActiveRecord::Migration
  def change
    create_table :ct_refs do |t|
      t.string :name, default: ""
      t.string :state, default: ""
      t.text :comment, default: ""
      t.string :summary, default: ""
      t.string :description, default: ""
      t.string :clinicaltrialgov_id, default: ""
      t.string :eudract_id, default: ""
      t.string :condition, default: ""
      t.string :sponsor_name, default: ""
      t.string :endpoint, default: ""
      t.string :role_of_device, default: ""
      t.references :device, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
