class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :name, default: ""
      t.string :state, default: ""
      t.text :comment, default: ""
      t.references :system, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
