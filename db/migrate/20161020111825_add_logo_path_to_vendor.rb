class AddLogoPathToVendor < ActiveRecord::Migration
  def change
    add_column :vendors, :logo_path, :string
    add_column :devices, :image_path, :string    
  end
end
