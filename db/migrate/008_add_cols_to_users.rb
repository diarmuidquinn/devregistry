class AddColsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :role, :integer
    # add_column :users, :organization_id, :integer
    add_reference :users, :organization, index: true
    
   
  end
end
