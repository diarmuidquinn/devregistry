class CreateMarketingRefs < ActiveRecord::Migration
  def change
    create_table :marketing_refs do |t|
      t.string :name, default: false
      t.string :state, default: false
      t.text :comment, default: false
      t.string :website, default: false
      t.string :summary, default: false
      t.string :description, default: false
      t.date :out_of_date
      t.date :last_vendor_update
      t.references :device, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
