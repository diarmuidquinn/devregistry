class CreatePeerReviewRefs < ActiveRecord::Migration
  def change
    create_table :peer_review_refs do |t|
      t.string :name, default: ""
      t.string :state, default: ""
      t.text :comment, default: ""
      t.string :summary, default: ""
      t.string :description, default: ""
      t.string :publication, default: ""
      t.string :reference, default: ""
      t.string :website, default: ""
      t.date :date_of_publication
      t.date :out_of_date
      t.date :last_vendor_update
      t.references :device, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
