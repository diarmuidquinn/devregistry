class CreateSystems < ActiveRecord::Migration
  def change
    create_table :systems do |t|
      t.string :name, default: ""
      t.string :state, default: ""
      t.text :comment, default: ""

      t.timestamps null: false
    end
  end
end
