# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161020111825) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "client_refs", force: :cascade do |t|
    t.string   "name",               default: ""
    t.string   "state",              default: ""
    t.text     "comment",            default: ""
    t.string   "division",           default: ""
    t.string   "website",            default: ""
    t.string   "summary",            default: ""
    t.text     "description",        default: ""
    t.string   "contact_name",       default: ""
    t.string   "contact_email",      default: ""
    t.boolean  "permitted",          default: false
    t.date     "out_of_date"
    t.date     "last_vendor_update"
    t.integer  "device_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "client_refs", ["device_id"], name: "index_client_refs_on_device_id", using: :btree

  create_table "ct_refs", force: :cascade do |t|
    t.string   "name",                default: ""
    t.string   "state",               default: ""
    t.text     "comment",             default: ""
    t.string   "summary",             default: ""
    t.string   "description",         default: ""
    t.string   "clinicaltrialgov_id", default: ""
    t.string   "eudract_id",          default: ""
    t.string   "condition",           default: ""
    t.string   "sponsor_name",        default: ""
    t.string   "endpoint",            default: ""
    t.string   "role_of_device",      default: ""
    t.integer  "device_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "ct_refs", ["device_id"], name: "index_ct_refs_on_device_id", using: :btree

  create_table "devices", force: :cascade do |t|
    t.string   "name",               default: ""
    t.string   "state",              default: ""
    t.text     "comment",            default: ""
    t.string   "summary",            default: ""
    t.text     "description",        default: ""
    t.string   "website",            default: ""
    t.string   "category",           default: ""
    t.string   "contact_name",       default: ""
    t.string   "contact_email",      default: ""
    t.date     "out_of_date"
    t.date     "last_vendor_update"
    t.integer  "vendor_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "image_path"
  end

  add_index "devices", ["vendor_id"], name: "index_devices_on_vendor_id", using: :btree

  create_table "marketing_refs", force: :cascade do |t|
    t.string   "name",               default: "f"
    t.string   "state",              default: "f"
    t.text     "comment",            default: "f"
    t.string   "website",            default: "f"
    t.string   "summary",            default: "f"
    t.string   "description",        default: "f"
    t.date     "out_of_date"
    t.date     "last_vendor_update"
    t.integer  "device_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "marketing_refs", ["device_id"], name: "index_marketing_refs_on_device_id", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.string   "name",       default: ""
    t.string   "state",      default: ""
    t.text     "comment",    default: ""
    t.integer  "system_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "organizations", ["system_id"], name: "index_organizations_on_system_id", using: :btree

  create_table "peer_review_refs", force: :cascade do |t|
    t.string   "name",                default: ""
    t.string   "state",               default: ""
    t.text     "comment",             default: ""
    t.string   "summary",             default: ""
    t.string   "description",         default: ""
    t.string   "publication",         default: ""
    t.string   "reference",           default: ""
    t.string   "website",             default: ""
    t.date     "date_of_publication"
    t.date     "out_of_date"
    t.date     "last_vendor_update"
    t.integer  "device_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "peer_review_refs", ["device_id"], name: "index_peer_review_refs_on_device_id", using: :btree

  create_table "systems", force: :cascade do |t|
    t.string   "name",       default: ""
    t.string   "state",      default: ""
    t.text     "comment",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.integer  "role"
    t.integer  "organization_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["organization_id"], name: "index_users_on_organization_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vendors", force: :cascade do |t|
    t.string   "name",               default: ""
    t.string   "state",              default: ""
    t.text     "comment",            default: ""
    t.string   "division",           default: ""
    t.string   "website",            default: ""
    t.string   "linkedin",           default: ""
    t.string   "summary",            default: ""
    t.text     "description",        default: ""
    t.string   "contact_name",       default: ""
    t.string   "contact_email",      default: ""
    t.date     "out_of_date"
    t.date     "last_vendor_update"
    t.integer  "user_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "logo_path"
  end

  add_index "vendors", ["user_id"], name: "index_vendors_on_user_id", using: :btree

  add_foreign_key "client_refs", "devices"
  add_foreign_key "ct_refs", "devices"
  add_foreign_key "devices", "vendors"
  add_foreign_key "marketing_refs", "devices"
  add_foreign_key "organizations", "systems"
  add_foreign_key "peer_review_refs", "devices"
  add_foreign_key "vendors", "users"
end
