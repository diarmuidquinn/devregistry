Rails.application.routes.draw do

  ##################################
  ### home
  ##################################
  root to: 'visitors#index'

  ##################################
  ### admin (Administrate)
  ##################################
  namespace :admin do

    DashboardManifest::DASHBOARDS.each do |dashboard_resource|
      resources dashboard_resource
    end
    #resources :users 
    #resources :organizations
    #resources :systems
    #resources :vendors
    #resources :devices
    #resources :ct_refs
    #resources :client_refs            
    root to: "users#index"
  end


  ####################
  ### visitors
  ####################
  get 'visitors/super_user_home', to: 'visitors#super_user_home'
  get 'visitors/admin_home', to: 'visitors#admin_home'
  get 'visitors/evaluator_home', to: 'visitors#evaluator_home'
  get 'visitors/vendor_home', to: 'visitors#vendor_home'
  get 'visitors/visitor_home', to: 'visitors#visitor_home'
  get 'visitors/waiting_approval_home', to: 'visitors#waiting_approval_home'
  get 'visitors/suspended_home', to: 'visitors#sustemded_home'      


  ####################
  ### client_refs
  ####################
  resources :client_refs
  # and for my children, allow me to create a new child
  resources "client_refs" do # client_ref_new_XXX_path 
  end

  ####################
  ### ct_refs
  ####################
  #match 'systems/:id/start_scheduler' => 'systems#start_scheduler', :via => :get, :as => 'system_start_scheduler'
  resources :ct_refs
  # and for my children, allow me to create a new child
  resources "ct_refs" do # ct_ref_new_XXX_path 
  end

  
  ####################
  ### devices
  ####################
  match 'devices/:id/my_show' => 'devices#my_show', :via => :get, :as => 'device_my_show'
  resources :devices
  # and for my children, allow me to create a new child
  resources "devices" do # device_new_XXX_path 
    post :new_ct_ref                           
    post :new_client_ref
    post :new_marketing_ref
    post :new_peer_reviewed_ref
  end
  
  ####################
  ### marketing_refs
  ####################
  resources :marketing_refs
  # and for my children, allow me to create a new child
  resources "marketing_refs" do # marketing_ref_new_XXX_path 
  end

  
  ####################
  ### organizations
  ####################
  resources :organizations
  # and for my children, allow me to create a new child
  resources "organizations" do
    post :new_user                           # organization_new_user_path 
                                             # POST /organizations/:organization_id/new_user(.:format)
    post :new_vendor
    member do
      # get  :register_email_intervention      # register_email_intervention_organization_path 
      #                                        # GET /organizations/:id/register_email_intervention(.:format)
    end
  end

  ####################
  ### peer_review_refs
  ####################
  resources :peer_review_refs
  # and for my children, allow me to create a new child
  resources "peer_review_refs" do # peer_review_ref_new_XXX_path 
  end
  
  
  ####################
  ### systems
  ####################
  #match 'systems/:id/start_scheduler' => 'systems#start_scheduler', :via => :get, :as => 'system_start_scheduler'
  resources :systems
  # and for my children, allow me to create a new child
  resources "systems" do
    post :new_organization
  end

  ##################################
  ### users
  ##################################
  devise_for :users, controllers: { sessions: 'users/sessions',
                                    registrations: 'users/registrations',
                                    passwords: 'users/passwords'}  
  # match 'users/for_approval' => 'users#for_approval', :via => :get #, :as => ''
  resources :users


  
  
  ####################
  ### vendors
  ####################
  match 'vendors/request_registration_1' => 'vendors#request_registration_1', :via => :get #, :as => ''
  match 'vendors/request_registration_2' => 'vendors#request_registration_2', :via => [:get] #, :as => ''
  match 'vendors/request_registration_3' => 'vendors#request_registration_3', :via => [:post] #, :as => ''
  match 'vendors/for_approval' => 'vendors#for_approval', :via => :get #, :as => ''
  match 'vendors/devices_for_approval' => 'vendors#devices_for_approval', :via => :get #, :as => ''  
  resources :vendors
  # and for my children, allow me to create a new child
  resources "vendors" do # maps to [vendor_my_view_path] [GET /vendors/:vendor_id/my_view] [vendors#my_view]
    get :my_view
    post :my_view
    patch :my_view
    get :my_new_device
    post :my_new_device
    get :show_for_approval
    get :approve
    get :reject
    get :suspend        
  end

  


  
end
