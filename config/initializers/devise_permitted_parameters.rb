module DevisePermittedParameters
  extend ActiveSupport::Concern

  included do
    before_action :configure_permitted_parameters
  end

  protected

  def configure_permitted_parameters
    # devise_parameter_sanitizer.for(:sign_up) << :name # devise v3
    # devise_parameter_sanitizer.for(:account_update) << :name # devise #v3
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name]) ## devise v4
    devise_parameter_sanitizer.permit(:account_update, keys: [:name]) ## devise v4
    
  end

end

DeviseController.send :include, DevisePermittedParameters
