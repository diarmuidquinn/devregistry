require "administrate/base_dashboard"

class CtRefDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    device: Field::BelongsTo,
    id: Field::Number,
    name: Field::String,
    state: Field::String.with_options(searchable: false),
    comment: Field::Text,
    summary: Field::String,
    description: Field::String,
    clinicaltrialgov_id: Field::String,
    eudract_id: Field::String,
    condition: Field::String,
    sponsor_name: Field::String,
    endpoint: Field::String,
    role_of_device: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :device,
    :id,
    :name,
    :state,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :device,
    :id,
    :name,
    :state,
    :comment,
    :summary,
    :description,
    :clinicaltrialgov_id,
    :eudract_id,
    :condition,
    :sponsor_name,
    :endpoint,
    :role_of_device,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :device,
    :name,
    :state,
    :comment,
    :summary,
    :description,
    :clinicaltrialgov_id,
    :eudract_id,
    :condition,
    :sponsor_name,
    :endpoint,
    :role_of_device,
  ].freeze

  # Overwrite this method to customize how ct refs are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(ct_ref)
  #   "CtRef ##{ct_ref.id}"
  # end
end
