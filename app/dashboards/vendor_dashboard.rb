require "administrate/base_dashboard"

class VendorDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    name: Field::String,
    devices: Field::HasMany,    
    user: Field::BelongsTo,    
    description: Field::Text,    
    state: Field::String.with_options(searchable: false),
    comment: Field::Text,
    division: Field::String,
    website: Field::String,
    linkedin: Field::String,
    summary: Field::String,
    logo_path: Field::String,
    contact_name: Field::String,
    contact_email: Field::String,
    out_of_date: Field::DateTime,
    last_vendor_update: Field::DateTime,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :state,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :user,
    :state,
    :comment,
    :division,
    :website,
    :linkedin,
    :logo_path,
    :summary,
    :description,
    :contact_name,
    :contact_email,
    :out_of_date,
    :last_vendor_update,
    :created_at,
    :updated_at,
    :devices
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :name,
    :user,
    :state,
    :comment,
    :division,
    :website,
    :linkedin,
    :logo_path,    
    :summary,
    :description,
    :contact_name,
    :contact_email,
    :out_of_date,
    :last_vendor_update,
  ].freeze

  # Overwrite this method to customize how vendors are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(vendor)
    "#{vendor.name}"
  end
end
