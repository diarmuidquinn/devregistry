require "administrate/base_dashboard"

class ClientRefDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    device: Field::BelongsTo,
    id: Field::Number,
    name: Field::String,
    state: Field::String,
    comment: Field::Text,
    division: Field::String,
    website: Field::String,
    summary: Field::String,
    description: Field::Text,
    contact_name: Field::String,
    contact_email: Field::String,
    permitted: Field::Boolean,
    out_of_date: Field::DateTime,
    last_vendor_update: Field::DateTime,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :device,
    :id,
    :name,
    :state,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :device,
    :id,
    :name,
    :state,
    :comment,
    :division,
    :website,
    :summary,
    :description,
    :contact_name,
    :contact_email,
    :permitted,
    :out_of_date,
    :last_vendor_update,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :device,
    :name,
    :state,
    :comment,
    :division,
    :website,
    :summary,
    :description,
    :contact_name,
    :contact_email,
    :permitted,
    :out_of_date,
    :last_vendor_update,
  ].freeze

  # Overwrite this method to customize how client refs are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(client_ref)
  #   "ClientRef ##{client_ref.id}"
  # end
end
