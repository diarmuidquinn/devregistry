class PeerReviewRef < ActiveRecord::Base


  ############################################
  ### Constants
  ############################################
  STATES = 
  [
    "Inactive",
    "Active"
  ]

  ############################################
  ### Callbacks
  ### http://api.rubyonrails.org/classes/ActiveRecord/Callbacks.html
  ############################################
  ### before_create :generate_permalink

  ############################################
  ### Validations
  ### http://guides.rubyonrails.org/active_record_validations.html 
  ############################################
  validates_presence_of  :name #, :state
  validates_uniqueness_of :name
  # validates :id, numericality: {equal_to: 1}  # there should only be one system
  ### validates :name, length: { minimum: 2 }
  validates :name, length: { maximum: 80 }
  ### validates :password, length: { in: 6..20 }
  ### validates :registration_number, length: { is: 6 }
  validates_inclusion_of :state, in: self::STATES
  ### validates :size, inclusion: { in: %w(small medium large),  message: "%{value} is not a valid size" }
  ### validates :legacy_code, format: { with: /\A[a-zA-Z]+\z/,   message: "only allows letters" }

  ############################################
  ### Associations
  ### http://guides.rubyonrails.org/association_basics.html
  ############################################
  belongs_to :device
  # has_many :users

  # has_and_belongs_to_many :codelocs
  ### has_many :assigned_tasks, :class_name => "Task", :foreign_key => "assigned_user_id"
  ### has_and_belongs_to_many :guardians  ## 2.335
  ### belongs_to :child, :class_name => "Member", :foreign_key => "child_id" ## Self-referential, p2.351
  ### has_many :parents, :class_name => "Member", :foreign_key => "parent_id"  ## Self-referential, p2.351
  ### has_many :blocks, :order => :position ## acts as list ref 2.352
  ### acts_as_list :scope => :block ## acts as list ref 2.352. Run ruby script/plugin install acts_as_list
  ### belongs_to :resource, :polymorphic => true ## polymorphic, ref 2.346
  ### has_one :question, :as => :resource ## 2.324, 2.328


  #################################################################################################
  ### Protected Region: Callbacks [1.p302]
  ################################################################################################# 
  protected

  ############################################
  ### get_all_mine
  ############################################ 
  def self.get_all_mine(in_current_user)
    instances = []
    if in_current_user.try(:super_user?)
      instances = self.all 
    else
      instances << in_current_user.organization.peer_review_refs      
    end
    instances
  end
end
