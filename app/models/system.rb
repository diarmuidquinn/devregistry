class System < ActiveRecord::Base

  @@scheduler_timer = nil
  @@scheduler_counter = 0

  ############################################
  ### Enums
  ### http://edgeapi.rubyonrails.org/classes/ActiveRecord/Enum.html
  ### why not to use: https://www.kcoleman.me/blog/2014/06/05/rails-4-dot-1-enums-how-not-to-handle-states/
  ############################################
  # enum state: [ :unconnected, :connected ]

  ############################################
  ### Constants
  ############################################
  STATES = 
  [
    "Inactive",
    "Active"
  ]

  ############################################
  ### Callbacks
  ### http://api.rubyonrails.org/classes/ActiveRecord/Callbacks.html
  ############################################
  ### before_create :generate_permalink

  ############################################
  ### Validations
  ### http://guides.rubyonrails.org/active_record_validations.html 
  ############################################
  validates_presence_of  :name #, :state
  validates_uniqueness_of :name
  # validates :id, numericality: {equal_to: 1}  # there should only be one system
  ### validates :name, length: { minimum: 2 }
  validates :name, length: { maximum: 80 }
  ### validates :password, length: { in: 6..20 }
  ### validates :registration_number, length: { is: 6 }
  validates_inclusion_of :state, in: System::STATES
  ### validates :size, inclusion: { in: %w(small medium large),  message: "%{value} is not a valid size" }
  ### validates :legacy_code, format: { with: /\A[a-zA-Z]+\z/,   message: "only allows letters" }

  ############################################
  ### Associations
  ### http://guides.rubyonrails.org/association_basics.html
  ############################################
  ### belongs_to :codeloc
  has_many :organizations, -> {order 'id'}, dependent: :destroy

  #has_many :my_programs, :through => :organizations


  # has_and_belongs_to_many :codelocs
  ### has_many :assigned_tasks, :class_name => "Task", :foreign_key => "assigned_user_id"
  ### has_and_belongs_to_many :guardians  ## 2.335
  ### belongs_to :child, :class_name => "Member", :foreign_key => "child_id" ## Self-referential, p2.351
  ### has_many :parents, :class_name => "Member", :foreign_key => "parent_id"  ## Self-referential, p2.351
  ### has_many :blocks, :order => :position ## acts as list ref 2.352
  ### acts_as_list :scope => :block ## acts as list ref 2.352. Run ruby script/plugin install acts_as_list
  ### belongs_to :resource, :polymorphic => true ## polymorphic, ref 2.346
  ### has_one :question, :as => :resource ## 2.324, 2.328


  ############################################
  ### uc_controller_log
  ############################################ 
  def self.uc_controller_log(in_uc, in_user, in_controller, in_action, in_extras)
    t = DateTime.now
    m = ":== UC_" + in_uc
    m += " == User=" + in_user.id.to_s if in_user
    m += " == Controller: " + in_controller + "#" + in_action + " == " + in_extras
    puts "\033[0;32m" + m + "\033[0m" # green
  end

  ############################################
  ### uc_view_log
  ############################################ 
  def self.uc_view_log(in_uc, in_user, in_controller, in_action, in_extras)
    t = DateTime.now
    m = ":== UC_" + in_uc
    m += " == User=" + in_user.id.to_s if in_user
    m += " == View: " + in_controller + "#" + in_action + " == " + in_extras
    puts "\033[0;32m" + m + "\033[0m" # green
  end
  

  ############################################
  ### log
  ###    Types are: 
  ###    --- WF: Workflow
  ###    --- ER: Error
  ###    --- DB: Debug
  ############################################ 
  def self.yylog(in_type, in_message)
    t = DateTime.now
    caller = caller_locations(1,1)[0].label
    # http://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
    message = " === " + caller + " === " + in_type + " === " + t.hour.to_s + ":" + t.minute.to_s + ":" + t.second.to_s + " ======> " + in_message
    puts "\033[0;32m" + message + "\033[0m"
    open('log.txt', 'a') do |f|
      f.puts message
    end 
  end

  ############################################
  ### log2
  ###    Types are: 
  ###    --- WF: Workflow
  ###    --- ER: Error
  ###    --- DB: Debug
  ############################################ 
  def self.yylog2(p = {})
    in_type = p[:type] || "XX"
    in_abc = p[:abc] || ""
    in_wf = p[:wf] || ""
    in_wf2 = p[:wf2] || ""    
    in_program_id = p[:program_id].to_s || ""
    in_p_module_id = p[:p_module_id].to_s || ""
    in_p_action_id = p[:p_action_id].to_s || ""        
    in_my_program_id = p[:my_program_id].to_s || ""
    in_my_module_id = p[:my_module_id].to_s || ""
    in_my_action_id = p[:my_action_id] || ""
    in_message = p[:message] || ""
    m = ""
    t = DateTime.now
    caller = caller_locations(1,1)[0].label
    # http://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
    m += ":== " + caller + " == " + in_type + in_abc + " == " + t.hour.to_s + ":" + t.minute.to_s + ":" + t.second.to_s + " ==> "
    m += "" + in_wf unless in_wf == ""
    m += " WF2:" + in_wf2 unless in_wf2 == ""    
    m += " !program:" + in_program_id unless in_program_id == ""
    m += " !p_module:" + in_p_module_id unless in_p_module_id == ""
    m += " !p_action:" + in_p_action_id unless in_p_action_id == ""
    m += " !my_program:" + in_my_program_id unless in_my_program_id == ""
    m += " !my_module:" + in_my_module_id unless in_my_module_id == ""
    m += " !my_action:" + in_my_action_id unless in_my_action_id == ""
    m += " :" + in_message unless in_message == ""        
    puts "\033[0;32m" + m + "\033[0m" if in_type == "WF"
    puts "\033[0;31m" + m + "\033[0m" if in_type == "ER"
    puts "\033[0;35m" + m + "\033[0m" if in_type == "DB"        
    ##open('log.txt', 'a') do |f|
    ##  f.puts message
    ##end 
  end


  #################################################################################################
  ### Protected Region: Callbacks [1.p302]
  ################################################################################################# 
  protected

  ############################################
  ### get_all_mine
  ############################################ 
  def self.get_all_mine(in_current_user)
     systems = []
     if in_current_user.try(:super_user?)
       systems = System.all 
     else
       systems << in_current_user.organization.system
     end
     systems
  end

end
