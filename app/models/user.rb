class User < ActiveRecord::Base
  enum role: [:super_user, :admin, :evaluator, :vendor, :visitor, :waiting_approval, :suspended]  
  after_initialize :set_default_role, :if => :new_record?

  belongs_to :organization
  has_many :vendors, -> {order 'id'}, dependent: :destroy
  
  def set_default_role
    self.role ||= :visitor
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable

  def timeout_in
    if self.admin? 
      20.minutes
    else
      20.minutes
    end
  end
  
end
