class UsersController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized

  def index
    # @users = User.all
    authorize User
    redirect_to home_path
  end

  def show
    @user = User.find(params[:id])
    authorize @user
  end

  def update
    @user = User.find(params[:id])
    authorize @user
    if @user.update_attributes(secure_params)
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end

  def destroy
    user = User.find(params[:id])
    authorize user
    user.destroy
    redirect_to users_path, :notice => "User deleted."
  end

  ###################################
  ### GET /users/for_approval
  ### helper: users_for_approva;_path
  ### params: {"controller"=>"users", "action"=>"for_approval"}
  ###################################
  #def for_approval
  #  # System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")    
  #  # @return_to = visitors_vendor_home_path
  #  @users_for_approval = User.where(role: "waiting_approval")
  #end

  
  private

  def secure_params
    params.require(:user).permit(:role)
  end

end
