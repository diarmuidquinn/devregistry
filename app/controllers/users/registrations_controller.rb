class Users::RegistrationsController < Devise::RegistrationsController
# before_action :configure_sign_up_params, only: [:create]
# before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    System.uc_controller_log("1.NewVisitor", current_user, controller_path, action_name, "")
    super
  end

  # POST /resource
  def create

    System.uc_controller_log("1.NewVisitor", current_user, controller_path, action_name, "")
    super
    if resource.persisted?
      org = Organization.find_by_name("Visitors")
      unless org
        org = Organization.new name: "Visitors", state: "Active", comment: "Autogenerated in registrations_controller.rb", system_id: 1
        org.save
        #System.uc_controller_log("1.a.NewVisitor", current_user, controller_path, action_name, "Couldn't find Visitors org")
        #redirect_to :back, notice: "Apologies, we could not sign you in at this time"
      end
      resource.organization = org
      resource.save
      m = "Visitor user id=" + resource.id.to_s + " created. Email is " + resource.email
      System.uc_controller_log("1.NewVisitor", current_user, controller_path, action_name, m)    
      # redirect_to root_path - there's a yield already in super. 
      
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
