class ClientRefsController < ApplicationController
  before_action :set_client_ref, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  after_action :verify_authorized
  

  # GET /client_refs
  # GET /client_refs.json
  def index
    @client_refs = ClientRef.all
  end

  # GET /client_refs/1
  # GET /client_refs/1.json
  def show
  end

  # GET /client_refs/new
  def new
    @client_ref = ClientRef.new
  end

  # GET /client_refs/1/edit
  def edit
  end

  # POST /client_refs
  # POST /client_refs.json
  def create
    @client_ref = ClientRef.new(client_ref_params)

    respond_to do |format|
      if @client_ref.save
        format.html { redirect_to @client_ref, notice: 'Client ref was successfully created.' }
        format.json { render :show, status: :created, location: @client_ref }
      else
        format.html { render :new }
        format.json { render json: @client_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /client_refs/1
  # PATCH/PUT /client_refs/1.json
  def update
    respond_to do |format|
      if @client_ref.update(client_ref_params)
        format.html { redirect_to @client_ref, notice: 'Client ref was successfully updated.' }
        format.json { render :show, status: :ok, location: @client_ref }
      else
        format.html { render :edit }
        format.json { render json: @client_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /client_refs/1
  # DELETE /client_refs/1.json
  def destroy
    @client_ref.destroy
    respond_to do |format|
      format.html { redirect_to client_refs_url, notice: 'Client ref was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_ref
      @client_ref = ClientRef.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_ref_params
      params.fetch(:client_ref, {})
    end
end
