class MarketingRefsController < ApplicationController
  before_action :set_marketing_ref, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  after_action :verify_authorized
  

  # GET /marketing_refs
  # GET /marketing_refs.json
  def index
    @marketing_refs = MarketingRef.all
  end

  # GET /marketing_refs/1
  # GET /marketing_refs/1.json
  def show
  end

  # GET /marketing_refs/new
  def new
    @marketing_ref = MarketingRef.new
  end

  # GET /marketing_refs/1/edit
  def edit
  end

  # POST /marketing_refs
  # POST /marketing_refs.json
  def create
    @marketing_ref = MarketingRef.new(marketing_ref_params)

    respond_to do |format|
      if @marketing_ref.save
        format.html { redirect_to @marketing_ref, notice: 'Marketing ref was successfully created.' }
        format.json { render :show, status: :created, location: @marketing_ref }
      else
        format.html { render :new }
        format.json { render json: @marketing_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /marketing_refs/1
  # PATCH/PUT /marketing_refs/1.json
  def update
    respond_to do |format|
      if @marketing_ref.update(marketing_ref_params)
        format.html { redirect_to @marketing_ref, notice: 'Marketing ref was successfully updated.' }
        format.json { render :show, status: :ok, location: @marketing_ref }
      else
        format.html { render :edit }
        format.json { render json: @marketing_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /marketing_refs/1
  # DELETE /marketing_refs/1.json
  def destroy
    @marketing_ref.destroy
    respond_to do |format|
      format.html { redirect_to marketing_refs_url, notice: 'Marketing ref was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marketing_ref
      @marketing_ref = MarketingRef.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def marketing_ref_params
      params.fetch(:marketing_ref, {})
    end
end
