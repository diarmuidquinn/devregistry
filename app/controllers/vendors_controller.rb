class VendorsController < ApplicationController
  before_action :set_vendor, only: [:show, :edit, :update, :destroy, :show_for_approval, :approve, :reject]
  before_action :authenticate_user!
  after_action :verify_authorized
  

  # GET /vendors
  # GET /vendors.json
  def index
    @vendors = Vendor.all
  end

  # GET /vendors/1
  # GET /vendors/1.json
  def show
  end

  # GET /vendors/new
  def new
    @vendor = Vendor.new
  end

  # GET /vendors/1/edit
  def edit
  end

  # POST /vendors
  # POST /vendors.json
  def create
    @vendor = Vendor.new(vendor_params)

    respond_to do |format|
      if @vendor.save
        format.html { redirect_to @vendor, notice: 'Vendor was successfully created.' }
        format.json { render :show, status: :created, location: @vendor }
      else
        format.html { render :new }
        format.json { render json: @vendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vendors/1
  # PATCH/PUT /vendors/1.json
  def update
    respond_to do |format|
      if @vendor.update(vendor_params)
        format.html { redirect_to @vendor, notice: 'Vendor was successfully updated.' }
        format.json { render :show, status: :ok, location: @vendor }
      else
        format.html { render :edit }
        format.json { render json: @vendor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vendors/1
  # DELETE /vendors/1.json
  def destroy
    @vendor.destroy
    respond_to do |format|
      format.html { redirect_to vendors_url, notice: 'Vendor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  ###################################
  ### GET /vendors/request_registration_1
  ### helper: vendors_request_registration_1_path
  ### params: {"controller"=>"vendors", "action"=>"request_registration_1"}
  ### request_registration_1
  ###################################
  def request_registration_1
    System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")        
    authorize Vendor
    
  end

  ###################################
  ### GET /vendors/request_registration_2
  ### helper: vendors_request_registration_2_path
  ### params: {"controller"=>"vendors", "action"=>"request_registration_2"}
  ### request_registration_2
  ###################################
  def request_registration_2
    System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")
    @vendor = Vendor.new
    authorize @vendor
    
  end

  ###################################
  ### GET /vendors/request_registration_3
  ### helper: vendors_request_registration_3_path
  ### params: {"controller"=>"vendors", "action"=>"request_registration_3"}
  ### request_registration_3
  ###################################
  def request_registration_3
    System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")
    @vendor = Vendor.new(vendor_params)
    @vendor.state = "SubmittedForReview"
    authorize @vendor
    @vendor.user = current_user
    if @vendor.save
      if current_user.role == "visitor"
        current_user.role = "vendor"
        current_user.save
      end
      redirect_to root_path      
    else
      render vendors_request_registration_2_path
      System.uc_controller_log("2.a.RequestVendorRegistration", current_user, controller_path, action_name, "Failed Vendor model validation")      
    end
    System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "Created Vendor=" + @vendor.id.to_s)
  end

  ###################################
  ### GET /vendors/for_approval
  ### helper: vendors_for_approva;_path
  ### params: {"controller"=>"vendors", "action"=>"for_approval"}
  ###################################
  def for_approval
    # System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")    
    # @return_to = visitors_vendor_home_path
    authorize Vendor    
    @vendors = Vendor.where(state: "SubmittedForReview").page(params[:page]).per(8)
    @return_to = params[:return_to] ||= root_path
    
  end

  ###################################
  ### GET /vendors/show_for_approval
  ### helper: vendors_show_for_approval_path(id)
  ### params: {"return_to"=>"uuu", "controller"=>"vendors", "action"=>"show_for_approval", "vendor_id"=>"2", "id"=>"2"}
  ###################################
  def show_for_approval
    # System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")    
    # @return_to = visitors_vendor_home_path
    authorize @vendor   
    @return_to = params[:return_to] ||= root_path    
  end

  ###################################
  ### GET /vendors/approve
  ### helper: vendors_approve_path(id)
  ### params: {"return_to"=>"uuu", "controller"=>"vendors", "action"=>"approve", "vendor_id"=>"2", "id"=>"2"}
  ###################################
  def approve
    # System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")    
    # @return_to = visitors_vendor_home_path
    authorize @vendor
    @vendor.state = "Active"
    if @vendor.save 
      flash[:notice] = "The Vendor #{@vendor.name} has been approved and can now register devices. An email has been sent to the requesting user. "
    else
      flash[:alert] = "The Vendor could not be approved - there was an error"          
    end
    redirect_to params[:return_to] ||= root_path    
  end

  ###################################
  ### GET /vendors/reject
  ### helper: vendors_reject_path(id)
  ### params: {"return_to"=>"uuu", "controller"=>"vendors", "action"=>"reject", "vendor_id"=>"2", "id"=>"2"}
  ###################################
  def reject
    # System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")    
    # @return_to = visitors_vendor_home_path
    authorize @vendor
    @vendor.state = "ApprovalRejected"
    if @vendor.save 
      flash[:notice] = "The Vendor #{@vendor.name} has been rejected and can now register devices. An email has been sent to the requesting user. "
    else
      flash[:alert] = "The Vendor could not be rejected - there was an error"          
    end
    redirect_to params[:return_to] ||= root_path    
  end

  ###################################
  ### GET /vendors/devices_for_approval
  ### helper: vendors_devices_for_approval_path
  ### params: {"controller"=>"vendors", "action"=>"devices_for_approval"}
  ###################################
  def devices_for_approval
    # System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")    
    # @return_to = visitors_vendor_home_path
    authorize Vendor

    @vendors_hash = {}

    
    @devices = Device.where(state: "SubmittedForReview")
    @number_of_devices_hash = {}
    for device in @devices
      @vendors_hash[device.vendor.id] = device.vendor
      if @number_of_devices_hash[device.vendor.id] == nil
        @number_of_devices_hash[device.vendor.id] = 1
      else
        @number_of_devices_hash[device.vendor.id] += 1
      end
    end
    
    @ct_refs = CtRef.where(state: "SubmittedForReview")
    @number_of_ct_refs_hash = {}
    for ct_ref in @ct_refs
      @vendors_hash[ct_ref.device.vendor.id] = ct_ref.device.vendor
      if @number_of_ct_refs_hash[ct_ref.device.vendor.id] == nil
        @number_of_ct_refs_hash[ct_ref.device.vendor.id] = 1
      else
        @number_of_ct_refs_hash[ct_ref.device.vendor.id] += 1
      end
    end
    
    @client_refs = ClientRef.where(state: "SubmittedForReview")
    @number_of_client_refs_hash = {}
    for client_ref in @client_refs
      @vendors_hash[client_ref.device.vendor.id] = client_ref.device.vendor
      if @number_of_client_refs_hash[client_ref.device.vendor.id] == nil
        @number_of_client_refs_hash[client_ref.device.vendor.id] = 1
      else
        @number_of_client_refs_hash[client_ref.device.vendor.id] += 1
      end
    end

    ###
    ### This must come last because we're counting vendors
    ###
    @vendors = []
    for key in @vendors_hash.keys
      @vendors << @vendors_hash[key]
    end
    # @vendors = @vendors.page(params[:page]).per(8)
    @vendors = Kaminari.paginate_array(@vendors).page(params[:page]).per(8)
    @return_to = params[:return_to] ||= root_path

  end
  
  
  ###################################
  ### GET /vendors/1/my_view
  ### helper: vendor_my_view_path
  ### GET params: {"controller"=>"vendors", "action"=>"my_view", "vendor_id"=>"22", "return_to"=>"/visitors/super_user_home"}
  ### my_view
  ###################################
  def my_view
    

    #############################
    ### Device-level actions
    #############################
    ### ct_ref_new
    ###
    #############################
    if params[:function] == "ct_ref_new"
      device = Device.find(params[:device_id])      
      if device.is_this_mine?(current_user)
        ct_ref = device.ct_refs.build(ct_ref_params)
        ct_ref.state = "Inactive"
        ct_ref.device = device
        if ct_ref.save
          flash[:notice] = "CT Reference was successfully created"
        else
          flash[:alert] = "CT Reference could not be created: " + ct_ref.errors.full_messages.join(",")
        end
        @ct_refs_hash[device.id] = device.ct_refs
      else
        flash[:alert] = "Error updating device - it's not yours!"
      end
    #############################
    ### client_ref_new
    ###
    #############################
    elsif params[:function] == "client_ref_new"
      device = Device.find(params[:device_id])      
      if device.is_this_mine?(current_user)
        client_ref = device.client_refs.build(client_ref_params)
        client_ref.state = "Inactive"
        client_ref.device = device
        if client_ref.save
          flash[:notice] = "Client Reference was successfully created"
        else
          flash[:alert] = "Client Reference could not be created: "  + client_ref.errors.full_messages.join(",")
        end
        @client_refs_hash[device.id] = device.client_refs
      else
        flash[:alert] = "Error updating device - it's not yours!"
      end
    #############################
    ### device_edit
    ###
    #############################
    elsif params[:function] == "device_edit"
      device = Device.find(params[:device_id])
      if device.is_this_mine?(current_user)
        if device.update(device_params)
          flash[:notice] = "Device <b>" + device.name + " </b>was successfully updated"
          flash[:notice] = flash[:notice].html_safe
        else
          flash[:alert] = "Device  <b>" + device.name + " </b>could not be updated: " + device.errors.full_messages.join(",")
          for message in device.errors.full_messages
            flash[:alert] += " --> " + message
          end
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error updating Device - it's not yours!"
      end
    #############################
    ### device_delete
    ###
    #############################
    elsif params[:function] == "device_delete"
      device = Device.find(params[:device_id])        
      if device.is_this_mine?(current_user)
        if params[:confirm_string] == "yes"
          device.destroy
          flash[:notice] = "Device <b>" + device.name + " </b>was deleted"
          flash[:notice] = flash[:notice].html_safe
        else
          flash[:alert] = "Device <b>" + device.name + " </b>was not deleted. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error deleting Device - it's not yours!"
      end
    #############################
    ### device_submit
    ###
    #############################
    elsif params[:function] == "device_submit"
      device = Device.find(params[:device_id])        
      if device.is_this_mine?(current_user)
        if params[:confirm_string] == "yes"
          device.state = "SubmittedForReview"
          if device.save
            flash[:notice] = "Device <b>" + device.name + " </b>has been submitted for review"
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:notice] = "Device <b>" + device.name + " </b>could not be submitted: " + device.errors.full_messages.join(",")
            flash[:notice] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "Device <b>" + device.name + " </b>was not submitted. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error submitting Device - it's not yours!"
      end
    #############################
    ### device_withdraw
    ###
    #############################
    elsif params[:function] == "device_withdraw"
      device = Device.find(params[:device_id])        
      if device.is_this_mine?(current_user)
        if params[:confirm_string] == "yes"
          device.state = "Inactive"
          if device.save
            flash[:notice] = "Device <b>" + device.name + " </b>has been withdrawn from review"
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:notice] = "Device <b>" + device.name + " </b>could not be withdrawn: " + device.errors.full_messages.join(",")
            flash[:notice] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "Device <b>" + device.name + " </b>was not withdrawn. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error withdrawing Device - it's not yours!"
      end

    #############################
    ### device_approve
    ###
    #############################
    elsif params[:function] == "device_approve"
      device = Device.find(params[:device_id])        
      if ["evaluator", "super_user"].include? current_user.role 
        if params[:confirm_string] == "yes"
          device.state = "Active"
          if device.save
            flash[:notice] = "Device <b>" + device.name + " </b>has been approved"
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:alert] = "Device <b>" + device.name + " </b>could not be approved: " + device.errors.full_messages.join(",")
            flash[:alert] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "Device <b>" + device.name + " </b>was not approved. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error approving Device - you don't have the rights!"
      end

    #############################
    ### device_reject
    ###
    #############################
    elsif params[:function] == "device_reject"
      device = Device.find(params[:device_id])        
      if ["evaluator", "super_user"].include? current_user.role 
        if params[:confirm_string] == "yes"
          device.state = "Inactive"
          if device.save
            flash[:notice] = "Device <b>" + device.name + " </b>has been rejected."
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:alert] = "Device <b>" + device.name + " </b>could not be rejected: " + device.errors.full_messages.join(",")
            flash[:alert] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "Device <b>" + device.name + " </b>was not rejected. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error rejecting Device - you don't have the rights!"
      end      

    #############################
    ### CtRef-level actions
    #############################
    ### ct_ref_edit
    ###
    #############################
    elsif params[:function] == "ct_ref_edit"
      ct_ref = CtRef.find(params[:ct_ref_id])
      if ct_ref.is_this_mine?(current_user)
        if ct_ref.update(ct_ref_params)
          flash[:notice] = "CT Reference <b>" + ct_ref.name + " </b>was successfully updated"
          flash[:notice] = flash[:notice].html_safe
        else
          flash[:alert] = "CT Reference  <b>" + ct_ref.name + " </b>could not be updated: " + ct_ref.errors.full_messages.join(",")
          for message in ct_ref.errors.full_messages
            flash[:alert] += " --> " + message
          end
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error updating CT Reference - it's not yours!"
      end
    #############################
    ### ct_ref_delete
    ###
    #############################
    elsif params[:function] == "ct_ref_delete"
      ct_ref = CtRef.find(params[:ct_ref_id])        
      if ct_ref.is_this_mine?(current_user)
        if params[:confirm_string] == "yes"
          ct_ref.destroy
          flash[:notice] = "CT Reference <b>" + ct_ref.name + " </b>was deleted"
          flash[:notice] = flash[:notice].html_safe
        else
          flash[:alert] = "CT Reference <b>" + ct_ref.name + " </b>was not deleted. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error deleting CT Reference - it's not yours!"
      end
    #############################
    ### ct_ref_submit
    ###
    #############################
    elsif params[:function] == "ct_ref_submit"
      ct_ref = CtRef.find(params[:ct_ref_id])        
      if ct_ref.is_this_mine?(current_user)
        if params[:confirm_string] == "yes"
          ct_ref.state = "SubmittedForReview"
          if ct_ref.save
            flash[:notice] = "CT Reference <b>" + ct_ref.name + " </b>has been submitted for review"
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:notice] = "CT Reference <b>" + ct_ref.name + " </b>could not be submitted: " + ct_ref.errors.full_messages.join(",")
            flash[:notice] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "CT Reference <b>" + ct_ref.name + " </b>was not submitted. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error submitting CT Reference - it's not yours!"
      end
    #############################
    ### ct_ref_withdraw
    ###
    #############################
    elsif params[:function] == "ct_ref_withdraw"
      ct_ref = CtRef.find(params[:ct_ref_id])        
      if ct_ref.is_this_mine?(current_user)
        if params[:confirm_string] == "yes"
          ct_ref.state = "Inactive"
          if ct_ref.save
            flash[:notice] = "CT Reference <b>" + ct_ref.name + " </b>has been withdrawn from review"
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:notice] = "CT Reference <b>" + ct_ref.name + " </b>could not be withdrawn: " + ct_ref.errors.full_messages.join(",")
            flash[:notice] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "CT Reference <b>" + ct_ref.name + " </b>was not withdrawn. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error withdrawing CT Reference - it's not yours!"
      end

    #############################
    ### ct_ref_approve
    ###
    #############################
    elsif params[:function] == "ct_ref_approve"
      ct_ref = CtRef.find(params[:ct_ref_id])        
      if ["evaluator", "super_user"].include? current_user.role 
        if params[:confirm_string] == "yes"
          ct_ref.state = "Active"
          if ct_ref.save
            flash[:notice] = "CT Reference <b>" + ct_ref.name + " </b>has been approved"
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:alert] = "CT Reference <b>" + ct_ref.name + " </b>could not be approved: " + ct_ref.errors.full_messages.join(",")
            flash[:alert] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "CT Reference <b>" + ct_ref.name + " </b>was not approved. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error approving CT Reference - you don't have the rights!"
      end

    #############################
    ### ct_ref_reject
    ###
    #############################
    elsif params[:function] == "ct_ref_reject"
      ct_ref = CtRef.find(params[:ct_ref_id])        
      if ["evaluator", "super_user"].include? current_user.role 
        if params[:confirm_string] == "yes"
          ct_ref.state = "Inactive"
          if ct_ref.save
            flash[:notice] = "CT Reference <b>" + ct_ref.name + " </b>has been rejected."
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:alert] = "CT Reference <b>" + ct_ref.name + " </b>could not be rejected: " + ct_ref.errors.full_messages.join(",")
            flash[:alert] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "CT Reference <b>" + ct_ref.name + " </b>was not rejected. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error rejecting CT Reference - you don't have the rights!"
      end      
    #############################
    ### ClientRef-level actions
    #############################
    ### client_ref_edit
    ###
    #############################
    elsif params[:function] == "client_ref_edit"
      client_ref = ClientRef.find(params[:client_ref_id])
      if client_ref.is_this_mine?(current_user)
        if client_ref.update(client_ref_params)
          flash[:notice] = "Client Reference <b>" + client_ref.name + " </b>was successfully updated"
          flash[:notice] = flash[:notice].html_safe
        else
          flash[:alert] = "Client Reference  <b>" + client_ref.name + " </b>could not be updated: " + client_ref.errors.full_messages.join(",")
          for message in client_ref.errors.full_messages
            flash[:alert] += " --> " + message
          end
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error updating Client Reference - it's not yours!"
      end
    #############################
    ### client_ref_delete
    ###
    #############################
    elsif params[:function] == "client_ref_delete"
      client_ref = ClientRef.find(params[:client_ref_id])        
      if client_ref.is_this_mine?(current_user)
        if params[:confirm_string] == "yes"
          client_ref.destroy
          flash[:notice] = "Client Reference <b>" + client_ref.name + " </b>was deleted"
          flash[:notice] = flash[:notice].html_safe
        else
          flash[:alert] = "Client Reference <b>" + client_ref.name + " </b>was not deleted. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error deleting Client Reference - it's not yours!"
      end
    #############################
    ### client_ref_submit
    ###
    #############################
    elsif params[:function] == "client_ref_submit"
      client_ref = ClientRef.find(params[:client_ref_id])        
      if client_ref.is_this_mine?(current_user)
        if params[:confirm_string] == "yes"
          client_ref.state = "SubmittedForReview"
          if client_ref.save
            flash[:notice] = "Client Reference <b>" + client_ref.name + " </b>has been submitted for review"
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:notice] = "Client Reference <b>" + client_ref.name + " </b>could not be submitted: " + client_ref.errors.full_messages.join(",")
            flash[:notice] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "Client Reference <b>" + client_ref.name + " </b>was not submitted. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error submitting Client Reference - it's not yours!"
      end
    #############################
    ### client_ref_withdraw
    ###
    #############################
    elsif params[:function] == "client_ref_withdraw"
      client_ref = ClientRef.find(params[:client_ref_id])        
      if client_ref.is_this_mine?(current_user)
        if params[:confirm_string] == "yes"
          client_ref.state = "Inactive"
          if client_ref.save
            flash[:notice] = "Client Reference <b>" + client_ref.name + " </b>has been withdrawn from review"
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:notice] = "Client Reference <b>" + client_ref.name + " </b>could not be withdrawn: " + client_ref.errors.full_messages.join(",")
            flash[:notice] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "Client Reference <b>" + client_ref.name + " </b>was not withdrawn. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error withdrawing Client Reference - it's not yours!"
      end

    #############################
    ### client_ref_approve
    ###
    #############################
    elsif params[:function] == "client_ref_approve"
      client_ref = ClientRef.find(params[:client_ref_id])        
      if ["evaluator", "super_user"].include? current_user.role 
        if params[:confirm_string] == "yes"
          client_ref.state = "Active"
          if client_ref.save
            flash[:notice] = "Client Reference <b>" + client_ref.name + " </b>has been approved"
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:alert] = "Client Reference <b>" + client_ref.name + " </b>could not be approved: " + client_ref.errors.full_messages.join(",")
            flash[:alert] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "Client Reference <b>" + client_ref.name + " </b>was not approved. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error approving Client Reference - you don't have the rights!"
      end

    #############################
    ### client_ref_reject
    ###
    #############################
    elsif params[:function] == "client_ref_reject"
      client_ref = ClientRef.find(params[:client_ref_id])        
      if ["evaluator", "super_user"].include? current_user.role 
        if params[:confirm_string] == "yes"
          client_ref.state = "Inactive"
          if client_ref.save
            flash[:notice] = "Client Reference <b>" + client_ref.name + " </b>has been rejected."
            flash[:notice] = flash[:notice].html_safe
          else
            flash[:alert] = "Client Reference <b>" + client_ref.name + " </b>could not be rejected: " + client_ref.errors.full_messages.join(",")
            flash[:alert] = flash[:notice].html_safe
          end
        else
          flash[:alert] = "Client Reference <b>" + client_ref.name + " </b>was not rejected. You did not enter the correct confirmation string."
          flash[:alert] = flash[:alert].html_safe
        end
      else
        flash[:alert] = "Error rejecting Client Reference - you don't have the rights!"
      end      
      
    #############################
    ### No action - just view
    #############################
    elsif params[:function] == nil
      # do nothing - just show the view

    #############################
    ### Unknown function
    #############################
    else
      flash[:alert] = "Could not understand the action: " + params[:function]
    end


    @vendor = Vendor.find(params[:vendor_id])
    authorize @vendor
    @category_dropdown = Device::CATEGORIES    
    @return_to = vendor_my_view_path(@vendor)
    @form_path = vendor_my_view_path(@vendor)
    @devices = @vendor.devices
    @ct_refs_hash = {}
    for device in @devices
      @ct_refs_hash[device.id] = device.ct_refs
      @client_refs_hash[device.id] = device.client_refs
      @marketing_refs_hash[device.id] = device.marketing_refs
      @peer_reviewed_refs_hash[device.id] = device.peer_reviewed_refs
    end

    # for the reviewers
    @devices_unreviewed_ct_refs = {}
    @devices_unreviewed_client_refs = {}    
    @devices_unreviewed = []
    @ct_refs_unreviewed = []
    @clientt_refs_unreviewed = []        
    for device in @devices
      if device.state == "SubmittedForReview"
        @devices_unreviewed << device.id
      end
      @devices_unreviewed_ct_refs[device.id] = CtRef.where(state: "SubmittedForReview", device_id: device.id)
      @devices_unreviewed_client_refs[device.id] = ClientRef.where(state: "SubmittedForReview", device_id: device.id)
    end
    
    
  end  

  ###################################
  ### GET /vendors/1/my_new_device
  ### helper: vendor_my_device_path
  ### GET params: {"controller"=>"vendors", "action"=>"my_new_device", "vendor_id"=>"22", "return_to"=>"/visitors/super_user_home"}
  ### my_new_device
  ###################################
  def my_new_device
  
    @vendor = Vendor.find(params[:vendor_id])
    authorize @vendor
    @form_path = vendor_my_new_device_path(@vendor)
    @return_to = params[:return_to] || root_path
    @category_dropdown = Device::CATEGORIES
    if request.get?
      @device = @vendor.devices.build
    else
      @device = Device.new(device_params)
      @device.vendor = @vendor
      @device.state = "Inactive"      
      if @device.save
        redirect_to @return_to, notice: 'Device was successfully created. It is Inactive, and should be submitted for review before it can be publishe'
      end
    end
  end  


  
  ###################################
  ### POST /vendors/1/my_new_device_create
  ### helper: vendor_my_new_device_create_path
  ### GET params: {"controller"=>"vendors", "action"=>"my_new_device_create", "vendor_id"=>"22", "return_to"=>"/visitors/super_user_home"}
  ### my_new_device_create
  ###################################
  def my_new_device_create

    @vendor = Vendor.find(params[:vendor_id])
    authorize @vendor        
    @device = Device.new(device_params)
    @device.state = "Inactive"
    @return_to = params[:return_to] || root_path    
    if @device.save
      redirect_to @return_to, notice: 'Device was successfully created. It is Inactive, and should be submitted for review before it can be published'
    else
      render vendor_my_new_device_path(@vendor), notice: 'Device was not created'
    end 
  end  

  
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vendor
      id = params[:id] ||= params[:vendor_id]
      @vendor = Vendor.find(id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vendor_params
      params[:vendor].permit(:name, :division, :website, :linkedin, :summary, :description, :contact_name, :contact_email, :comment)      
    end
    def device_params
      # http://edgeapi.rubyonrails.org/classes/ActionController/StrongParameters.html
     params[:device].permit(:name, :summary, :description, :website, :category, :contact_name, :contact_email, :comment)            
    end
    def ct_ref_params
      # http://edgeapi.rubyonrails.org/classes/ActionController/StrongParameters.html
     params[:ct_ref].permit(:name, :summary, :description, :clinicaltrialgov_id, :eudract_id, :condition, :sponsor_name, :endpoint, :role_of_device)            
    end
    def client_ref_params
      # http://edgeapi.rubyonrails.org/classes/ActionController/StrongParameters.html
     params[:client_ref].permit(:name, :summary, :description, :division, :website, :contact_name, :contact_email, :permitted)            
    end

end
