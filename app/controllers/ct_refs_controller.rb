class CtRefsController < ApplicationController
  before_action :set_ct_ref, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  after_action :verify_authorized
  

  # GET /ct_refs
  # GET /ct_refs.json
  def index
    @ct_refs = CtRef.all
  end

  # GET /ct_refs/1
  # GET /ct_refs/1.json
  def show
  end

  # GET /ct_refs/new
  def new
    @ct_ref = CtRef.new
  end

  # GET /ct_refs/1/edit
  def edit
  end

  # POST /ct_refs
  # POST /ct_refs.json
  def create
    @ct_ref = CtRef.new(ct_ref_params)

    respond_to do |format|
      if @ct_ref.save
        format.html { redirect_to @ct_ref, notice: 'Ct ref was successfully created.' }
        format.json { render :show, status: :created, location: @ct_ref }
      else
        format.html { render :new }
        format.json { render json: @ct_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ct_refs/1
  # PATCH/PUT /ct_refs/1.json
  def update
    respond_to do |format|
      if @ct_ref.update(ct_ref_params)
        format.html { redirect_to @ct_ref, notice: 'Ct ref was successfully updated.' }
        format.json { render :show, status: :ok, location: @ct_ref }
      else
        format.html { render :edit }
        format.json { render json: @ct_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ct_refs/1
  # DELETE /ct_refs/1.json
  def destroy
    @ct_ref.destroy
    respond_to do |format|
      format.html { redirect_to ct_refs_url, notice: 'Ct ref was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ct_ref
      @ct_ref = CtRef.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ct_ref_params
      params.fetch(:ct_ref, {})
    end
end
