class VisitorsController < ApplicationController
  # before_action :set_vendor, only: [:show, :edit, :update, :destroy, :show_for_approval, :approve, :reject]
  before_action :authenticate_user!
  # after_action :verify_authorized  

  ###################################
  ### index
  ###   root to: 'visitors#index' 
  ###################################
  def index
    System.uc_controller_log("1.NewVisitor", current_user, controller_path, action_name, "")
    System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")    
    puts caller_locations(1,1)[0].label

    # if there are parameters I waant to pass them (except controller and action)
    params.delete("controller")
    params.delete("action")
    if current_user.try(:super_user?)
      redirect_to visitors_super_user_home_path(params)
    elsif current_user.try(:admin?)
      redirect_to visitors_admin_home_path(params)
    elsif current_user.try(:evaluator?)
      redirect_to visitors_evaluator_home_path(params)
    elsif current_user.try(:vendor?)
      redirect_to visitors_vendor_home_path(params)
    elsif current_user.try(:visitor?)
      redirect_to visitors_visitor_home_path(params)
    elsif current_user.try(:waiting_approval?)
      redirect_to visitors_waiting_approval_home_path(params)
    elsif current_user.try(:suspended?)
      redirect_to visitors_suspended_home_path(params)
    else
      redirect_to new_user_session_path
    end
  end
  
  ###################################
  ### GET /visitors/super_user_home
  ### helper: visitors_super_user_home_path
  ### params: {"controller"=>"visitors", "action"=>"super_user_home"}
  ### super_user_home
  ###################################
  def super_user_home
    @here = visitors_super_user_home_path
    @organization = current_user.organization
    if @organization.name == "3rdPillar"
      @vendors = Vendor.all.page(params[:page]).per(9)
      @users = User.all.page(params[:page]).per(9)
      @organizations = Organization.all.page(params[:page]).per(9)      
    else
      @vendors = @organization.vendors.page(params[:page]).per(9)
      @users = @organization.users.all.page(params[:page]).per(9)
      @organizations = []
      @organizations << @organization
     
    end

  end

  ###################################
  ### GET /visitors/visitor_home
  ### helper: visitors_visitor_home_path
  ### params: {"controller"=>"visitors", "action"=>"visitor_home"}
  ### super_user_home
  ###################################
  def visitor_home
    System.uc_controller_log("1.NewVisitor", current_user, controller_path, action_name,  "")    
    @here = visitors_visitor_home_path
    @organization = current_user.organization

    @devices = my_home_filters
  end


  ###################################
  ### GET /visitors/vendor_home
  ### helper: visitors_vendor_home_path
  ### params: {"controller"=>"visitors", "action"=>"vendor_home"}
  ### vendor_home
  ###################################
  def vendor_home
    System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")    
    # @return_to = visitors_vendor_home_path
    @vendor = current_user.vendors[0] # there should only be 1 vendor
    @here = visitors_visitor_home_path
    @organization = current_user.organization

    @devices = my_home_filters
    
  end

  ###################################
  ### GET /visitors/evaluator_home
  ### helper: visitors_evaluator_home_path
  ### params: {"controller"=>"visitors", "action"=>"evaluator_home"}
  ###################################
  def evaluator_home
    # System.uc_controller_log("2.RequestVendorRegistration", current_user, controller_path, action_name, "")    
    @return_to = visitors_evaluator_home_path

    @devices = my_home_filters
    
    @vendors_for_review = Vendor.where(state: "SubmittedForReview")
    @devices_for_review = Device.where(state: "SubmittedForReview")
    @ct_refs_for_review = CtRef.where(state: "SubmittedForReview")
    @client_refs_for_review = ClientRef.where(state: "SubmittedForReview")        
  end
  

  #############################################################################################
  ### Private
  #############################################################################################
  private
  def my_home_filters

    if params[:category]
      devices = Device.where(state: "Active", category: params[:category])
    else
      devices = Device.where(state: "Active")
    end


    if params[:commit] == "Clear"
      params[:search] = nil
    end
    
    if params[:search]
      if params[:commit] == "Vendors"
        vendors = Vendor.basic_search(params[:search])
        for vendor in vendors
          devices += vendor.devices
        end
      end      
      if params[:commit] == "Devices"
        devices = Device.basic_search(params[:search])
      end      
      @search = params[:search]
    end
    if devices.class == Array
      list = Kaminari.paginate_array(devices).page(params[:page]).per(9)
    else
      list = devices.page(params[:page]).per(9)
    end

    list
  end

  
end
