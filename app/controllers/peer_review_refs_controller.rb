class PeerReviewRefsController < ApplicationController
  before_action :set_peer_review_ref, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  after_action :verify_authorized
  

  # GET /peer_review_refs
  # GET /peer_review_refs.json
  def index
    @peer_review_refs = PeerReviewRef.all
  end

  # GET /peer_review_refs/1
  # GET /peer_review_refs/1.json
  def show
  end

  # GET /peer_review_refs/new
  def new
    @peer_review_ref = PeerReviewRef.new
  end

  # GET /peer_review_refs/1/edit
  def edit
  end

  # POST /peer_review_refs
  # POST /peer_review_refs.json
  def create
    @peer_review_ref = PeerReviewRef.new(peer_review_ref_params)

    respond_to do |format|
      if @peer_review_ref.save
        format.html { redirect_to @peer_review_ref, notice: 'Peer review ref was successfully created.' }
        format.json { render :show, status: :created, location: @peer_review_ref }
      else
        format.html { render :new }
        format.json { render json: @peer_review_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /peer_review_refs/1
  # PATCH/PUT /peer_review_refs/1.json
  def update
    respond_to do |format|
      if @peer_review_ref.update(peer_review_ref_params)
        format.html { redirect_to @peer_review_ref, notice: 'Peer review ref was successfully updated.' }
        format.json { render :show, status: :ok, location: @peer_review_ref }
      else
        format.html { render :edit }
        format.json { render json: @peer_review_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /peer_review_refs/1
  # DELETE /peer_review_refs/1.json
  def destroy
    @peer_review_ref.destroy
    respond_to do |format|
      format.html { redirect_to peer_review_refs_url, notice: 'Peer review ref was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_peer_review_ref
      @peer_review_ref = PeerReviewRef.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def peer_review_ref_params
      params.fetch(:peer_review_ref, {})
    end
end
