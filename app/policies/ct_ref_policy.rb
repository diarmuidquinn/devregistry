class CtRefPolicy 
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @model = model
  end

  def index?
    @current_user.super_user?
  end

  def show?
    @current_user.super_user?
  end

  def update?
    @current_user.super_user?
  end

  def destroy?
    @current_user.super_user?
  end
end
