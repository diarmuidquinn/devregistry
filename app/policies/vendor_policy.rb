class VendorPolicy 
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @model = model
  end

  def index?
    @current_user.super_user?
  end

  def show?
    @current_user.super_user?
  end

  def update?
    (@current_user.vendor? && @model.is_this_mine?(current_user)) || @current_user.super_user?
  end

  def destroy?
    @current_user.super_user?
  end

  def request_registration_1?
    @current_user.visitor? || @current_user.super_user? 
  end

  def request_registration_2?
    @current_user.visitor? || @current_user.super_user? 
  end
  
  def request_registration_3?
    @current_user.visitor? || @current_user.super_user? 
  end

  def for_approval?
    @current_user.evaluator? || @current_user.super_user? 
  end

  def show_for_approval?
    @current_user.evaluator? || @current_user.super_user? 
  end

  def approve?
    @current_user.evaluator? || @current_user.super_user? 
  end

  def reject?
    @current_user.evaluator? || @current_user.super_user? 
  end

  def devices_for_approval?
    @current_user.evaluator? || @current_user.super_user? 
  end
 
  def my_view?
    (@current_user.vendor? && @model.is_this_mine?(current_user) ) || @current_user.super_user? || @current_user.evaluator? || @current_user.admin? 
  end

  def my_new_device?
    (@current_user.vendor? && @model.is_this_mine?(current_user) ) || @current_user.super_user?  
  end
  
  def my_new_device_create?
    (@current_user.vendor? && @model.is_this_mine?(current_user) ) || @current_user.super_user?  
  end
  

end
