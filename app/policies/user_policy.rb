class UserPolicy
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @user = model
  end

  def index?
    return true # always redirected to home_path
  end

  def show?
    @current_user.super_user? or @current_user == @user
  end

  def update?
    @current_user.super_user?
  end

  def destroy?
    return false if @current_user == @user
    @current_user.super_user?
  end

end
