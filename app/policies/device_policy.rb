class DevicePolicy
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @model = model
  end

  def index?
    @current_user.super_user?
  end

  def create?
    @current_user.super_user? || @current_user.vendor?
  end

  def show?
    @current_user.super_user?
  end

  def update?
    @current_user.super_user?
  end

  def destroy?
    @current_user.super_user?
  end

  def evaluate?
    @current_user.super_user? || @current_user.evaluator?
  end

  def my_show?
    true # all users can see it.
  end
  
end
