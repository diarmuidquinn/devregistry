json.extract! ct_ref, :id, :created_at, :updated_at
json.url ct_ref_url(ct_ref, format: :json)