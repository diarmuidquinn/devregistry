json.extract! marketing_ref, :id, :created_at, :updated_at
json.url marketing_ref_url(marketing_ref, format: :json)