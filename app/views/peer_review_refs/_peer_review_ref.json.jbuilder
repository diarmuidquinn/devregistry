json.extract! peer_review_ref, :id, :created_at, :updated_at
json.url peer_review_ref_url(peer_review_ref, format: :json)