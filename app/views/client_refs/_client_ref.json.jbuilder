json.extract! client_ref, :id, :created_at, :updated_at
json.url client_ref_url(client_ref, format: :json)