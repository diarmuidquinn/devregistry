FactoryGirl.define do
  factory :ct_ref do
    name "MyString"
    state "MyString"
    comment "MyText"
    summary "MyString"
    description "MyString"
    clinicaltrialgov_id "MyString"
    eudract_id "MyString"
    condition "MyString"
    sponsor_name "MyString"
    endpoint "MyString"
    role_of_device "MyString"
    device nil
  end
end
