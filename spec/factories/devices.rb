FactoryGirl.define do
  factory :device do
    name "MyString"
    state "MyString"
    comment "MyText"
    summary "MyString"
    description "MyText"
    website "MyString"
    category "MyString"
    contact_name "MyString"
    contact_email "MyString"
    out_of_date "2016-07-28"
    last_vendor_update "2016-07-28"
    vendor nil
  end
end
