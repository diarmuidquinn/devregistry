FactoryGirl.define do
  factory :vendor do
    name "MyString"
    state "MyString"
    comment "MyText"
    division "MyString"
    website "MyString"
    linkedin "MyString"
    summary "MyString"
    description "MyString"
    contact_name "MyString"
    contact_email "MyString"
    out_of_date "2016-07-28"
    last_vendor_update "2016-07-28"
    organization nil
  end
end
