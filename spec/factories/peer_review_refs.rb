FactoryGirl.define do
  factory :peer_review_ref do
    name "MyString"
    state "MyString"
    comment "MyText"
    summary "MyString"
    description "MyString"
    publication "MyString"
    reference "MyString"
    website "MyString"
    date_of_publication "MyString"
    contact_name "MyString"
    contact_email "MyString"
    out_of_date "2016-07-28"
    last_vendor_update "2016-07-28"
    device nil
  end
end
