FactoryGirl.define do
  factory :client_ref do
    name "MyString"
    state "MyString"
    comment "MyText"
    division "MyString"
    website "MyString"
    summary "MyString"
    description "MyText"
    contact_name "MyString"
    contact_email "MyString"
    permitted false
    out_of_date "2016-07-28"
    last_vendor_update "2016-07-28"
    device nil
  end
end
