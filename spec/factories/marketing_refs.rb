FactoryGirl.define do
  factory :marketing_ref do
    name "MyString"
    state "MyString"
    comment "MyText"
    website "MyString"
    summary "MyString"
    description "MyString"
    out_of_date "2016-07-28"
    last_vendor_update "2016-07-28"
    device nil
  end
end
